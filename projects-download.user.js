// ==UserScript==
// @name         Gestion de projets HEIA-FR
// @namespace    https://www.heia-fr.ch/
// @version      1.2
// @description  Enable JSON Download of projects!
// @author       Jacques Supcik <jacques.supcik@hefr.ch>
// @match        https://webapp.heia-fr.ch/gestionprojets/private/rechercher.jsp
// @grant        none
// @require      https://code.jquery.com/jquery-3.2.1.min.js
// @run-at       document-start
// @updateURL    https://gitlab.forge.hefr.ch/jacques.supcik/ged-tweak/raw/master/projects-download.user.js
// ==/UserScript==

function trim(x) {
    return x.replace(/^((<br>)|\s)+|((<br>)|\s)+$/g, "");
}

function getList(x) {
    if (typeof x == 'undefined') return [];
    return trim(x).split("<br>");
}

function getText(x) {
    if (typeof x == 'undefined') return "";
    return trim(x).replace(/<br>/g, " / ");
}

function ExtractJson() {
    var t0 = $("body table:first");
    var titles = t0.find("h2");

    var result = [];

    titles.each(function () {
        var o = {
            title: getText($(this).text()),
            abbrev: getText($(this).next("h3").text()),
        };

        var h = $(this).prev("table").find("tr:first").children("td");
        o.department = getList($(h).eq(0).find("td").html());
        o.type = getText($(h).eq(2).find("td").html());
        o.status = getText($(h).eq(4).text());

        var b = $(this).nextAll("table:first");
        //o.body = btoa($(b).find("tr:first td table td").html());
        o.body = $(b).find("tr:first td table td").html();

        var b1 = $(b).find("tr:gt(0)");
        o.re = getText($(b1).find("td:contains('Responsables externes')").next().text());
        o.ri = getList($(b1).find("td:contains('Responsables internes')").next().html());
        o.client = getText($(b1).find("td:contains('Proposé par')").next().text());
        o.max_stud = parseInt(getText($(b1).find("td:contains('Nombre d\\'étudiants')").next().text()));
        o.kw = getText($(b1).find("td:contains('Mots clés')").next().text()).replace(/,\s+/g, ", ");
        o.students = getList($(b1).find("td:contains('Etudiants inscrits')").next().html());
        o.links = $.map($(b1).find("td:contains('Liens')").next().find("a"),
            function (i) {
                return {href: $(i).attr("href"), text: $(i).text()};
            });

        result.push(o);
    });

    return new Blob([JSON.stringify(result, null, 2)], {type: "application/json"});
}

(function() {
    'use strict';
    if ($("body p:nth-child(2):contains('Imprimer')").length === 0 || $("form:contains('Affichage complet')").length > 0) {
        return;
    }
    $("body p:nth-child(2):contains('Imprimer'):first").append(' | <span id="buildingJSON">Building JSON...</span>');

    $(function () {
        var blob = ExtractJson();
        $("#buildingJSON").replaceWith('<a id="downloadJSON" href="#">Download JSON</span>');
        $("#downloadJSON").attr("href", URL.createObjectURL(blob));
        $("#downloadJSON").attr("download", "projects_heiafr.json");
    });

})();
